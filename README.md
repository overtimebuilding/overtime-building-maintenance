We are a leading Vancouver area commercial cleaning company, equipped to handle all your janitorial needs, from regular daily cleaning to weekly cleaning service and everything in-between. We are fully certified, bonded, insured and backed by a 100% Satisfaction Guarantee!

Address: 10114 King George Blvd, #202, Surrey, BC V3T 2W4, Canada

Phone: 604-988-8867

Website: https://overtimebm.com

